# AWS-Lambda-Function

-----------------------------Installation in Windows-------------------------------------------

1. Install the AWS CLI using python and pip on Windows
      -Go to https://www.python.org/downloads/windows/ to download Python Windows
       x86-64 installer for local machine.

      - Run the installer and choose Add Python 3 to path.

      - Install the software.


2. To install the AWS CLI with pip3 (Windows)
      
      - Open command prompt and write 

          C:\> pip3 install awscli

      - After installation verify that the AWS CLI is installed correctly

          C:\> aws --version
          aws-cli/1.16.71 Python/3.6.5 Windows/10 botocore/1.12.61

      - To upgrade to the latest version, run the installation command again.

          C:\> pip3 install --user --upgrade awscli


3. To install latest Javascript
  
      - Open the command line in administrator and paste 

        C:\>@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

    Wait a few seconds for the command to complete.


-----------------------------Installation in MAC-------------------------------------------

1. Install awscli for command line AWS
     
     brew install awscli

2 Install jq for command line Javascript parsing:
      
     brew install jq
